const passport = require('passport');
const passportJwt = require('passport-jwt');

const config = require('./env');

const { Strategy, ExtractJwt } = passportJwt;

module.exports = (app) => {
  const params = {
    secretOrKey: config.secret_token,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  };

  passport.use(
    new Strategy(params, (payload, done) => {
      app.api.services.user
        .findByIdWithAllField(payload.id)
        .then((user) => {
          if (!user) {
            done(null, false);
          } else {
            done(null, { ...payload });
          }
        })
        .catch((error) => {
          done(error, false);
        });
    }),
  );

  return {
    authenticate: () => passport.authenticate('jwt', { session: false }),
  };
};
