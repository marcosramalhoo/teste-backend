const Sequelize = require('sequelize');
const dbConfig = require('../config/env');

const connection = new Sequelize(dbConfig);

const User = require('../api/models/user');
const Director = require('../api/models/director');
const Genre = require('../api/models/genre');
const Movie = require('../api/models/movie');
const Vote = require('../api/models/vote');

User.init(connection);
Director.init(connection);
Genre.init(connection);
Movie.init(connection);
Vote.init(connection);

User.associate(connection.models);
Director.associate(connection.models);
Movie.associate(connection.models);
Genre.associate(connection.models);
Vote.associate(connection.models);

module.exports = connection;
