const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('users', [
      {
        usu_name: 'Admin',
        usu_password: bcrypt.hashSync('admin123', 10),
        usu_username: 'admin',
        usu_email: 'admin@admin.com',
        usu_isAdmin: 'S',
        createdAt: new Date(),
      },
      {
        usu_name: 'Admin Auxiliar',
        usu_password: bcrypt.hashSync('adminaux123', 10),
        usu_username: 'adminaux',
        usu_email: 'adminaux@admin.com',
        usu_isAdmin: 'S',
        createdAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('users', null, {});
  },
};
