module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('genres', [
      {
        gen_name: 'Animação',
        createdAt: new Date(),
      },
      {
        gen_name: 'Ação',
        createdAt: new Date(),
      },
      {
        gen_name: 'Comédia',
        createdAt: new Date(),
      },
      {
        gen_name: 'Documentário',
        createdAt: new Date(),
      },
      {
        gen_name: 'Drama',
        createdAt: new Date(),
      },
      {
        gen_name: 'Ficção Científica',
        createdAt: new Date(),
      },
      {
        gen_name: 'Guerra',
        createdAt: new Date(),
      },
      {
        gen_name: 'Terror',
        createdAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('genres', null, {});
  },
};
