module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('directors', [
      {
        dir_name: 'Martin Scorsese',
        createdAt: new Date(),
      },
      {
        dir_name: 'Francis Ford Coppola',
        createdAt: new Date(),
      },
      {
        dir_name: 'Stanley Kubrick',
        createdAt: new Date(),
      },
      {
        dir_name: 'Quentin Tarantino',
        createdAt: new Date(),
      },
      {
        dir_name: 'Woody Allen',
        createdAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('directors', null, {});
  },
};
