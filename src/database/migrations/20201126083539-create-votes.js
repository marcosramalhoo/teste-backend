module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('votes', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      usu_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'users', key: 'usu_id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        validate: {
          notEmpty: true,
        },
      },
      mov_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'movies', key: 'mov_id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        validate: {
          notEmpty: true,
        },
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      rating: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
      },
    });
  },

  down: async (queryInterface) => {
    return queryInterface.dropTable('votes');
  },
};
