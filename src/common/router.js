const express = require('express');

module.exports = (app) => {
  app.use('/auth', app.api.routes.auth);
  app.use('/api/v1/movies', app.api.routes.movie);

  const protectedRouter = express.Router();

  protectedRouter.use('/admins', app.api.routes.admin);
  protectedRouter.use('/users', app.api.routes.user);

  app.use('/api/v1', app.config.passport.authenticate(), protectedRouter);
};
