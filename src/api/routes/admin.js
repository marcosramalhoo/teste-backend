const express = require('express');

module.exports = (app) => {
  const router = express.Router();
  router.post(
    '/',
    app.api.middlewares.authJwt.isAdmin,
    app.api.controllers.admin.create,
  );
  router.put(
    '/:id',
    app.api.middlewares.authJwt.isAdmin,
    app.api.controllers.admin.update,
  );
  router.delete(
    '/:id',
    app.api.middlewares.authJwt.isAdmin,
    app.api.controllers.admin.remove,
  );

  return router;
};
