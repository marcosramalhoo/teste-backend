const express = require('express');

module.exports = (app) => {
  const router = express.Router();
  router.post(
    '/',
    [app.config.passport.authenticate(), app.api.middlewares.authJwt.isAdmin],
    app.api.controllers.movie.create,
  );
  router.delete(
    '/:id',
    [app.config.passport.authenticate(), app.api.middlewares.authJwt.isAdmin],
    app.api.controllers.movie.remove,
  );
  router.put(
    '/:id',
    [app.config.passport.authenticate(), app.api.middlewares.authJwt.isAdmin],
    app.api.controllers.movie.update,
  );
  router.post(
    '/votes',
    [app.config.passport.authenticate(), app.api.middlewares.authJwt.isUser],
    app.api.controllers.movie.votes,
  );
  router.get('', app.api.controllers.movie.index);
  router.get('/:id', app.api.controllers.movie.show);

  return router;
};
