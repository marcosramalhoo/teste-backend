const express = require('express');

module.exports = (app) => {
  const router = express.Router();
  router.put(
    '/:id',
    app.api.middlewares.authJwt.isAdmin,
    app.api.controllers.user.update,
  );
  router.delete(
    '/:id',
    app.api.middlewares.authJwt.isAdmin,
    app.api.controllers.user.remove,
  );

  return router;
};
