const bcrypt = require('bcrypt');

const User = require('../models/user');

module.exports = {
  async create(data) {
    return User.create(data);
  },

  async findByEmail(email) {
    return User.findOne({
      where: {
        email,
        deletedAt: null,
      },
    });
  },

  async findByIdWithAllField(userId) {
    return User.findOne({
      where: {
        usu_id: userId,
        deletedAt: null,
      },
    });
  },

  async findAdmin(userId) {
    return User.findOne({
      where: {
        usu_id: userId,
        usu_isAdmin: 'S',
        deletedAt: null,
      },
    });
  },

  async update(id, user) {
    return User.update(user, {
      where: {
        userId: id,
      },
      returning: true,
      plain: true,
    });
  },

  async delete(userId) {
    return User.update(
      {
        deletedAt: new Date(),
      },
      {
        where: { userId },
      },
    );
  },

  hashPassword(passwd) {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(passwd, salt);
  },
};
