const Genre = require('../models/genre');

module.exports = {
  async create(data) {
    return Genre.create(data);
  },
};
