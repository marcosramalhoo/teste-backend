const Director = require('../models/director');

module.exports = {
  async create(data) {
    return Director.create(data);
  },
};
