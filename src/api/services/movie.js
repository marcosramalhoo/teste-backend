const { Op } = require('sequelize');

const Director = require('../models/director');
const Genre = require('../models/genre');
const Movie = require('../models/movie');
const Vote = require('../models/vote');

module.exports = {
  async create(data) {
    return Movie.create(data);
  },

  async delete(movieId) {
    return Movie.update(
      {
        deletedAt: new Date(),
      },
      {
        where: { movieId },
      },
    );
  },

  async update(id, movie) {
    return Movie.update(movie, {
      where: { movieId: id },
      returning: true,
      plain: true,
    });
  },

  async findAll(filter) {
    const where = {
      deletedAt: {
        [Op.ne]: null,
      },
      [Op.or]: [],
    };

    if (filter.title) {
      where[Op.or].push({
        '$Movie.mov_title$': {
          [Op.iLike]: `%${filter.title}%`,
        },
      });
    }

    if (filter.genre) {
      where[Op.or].push({
        '$genre.gen_name$': {
          [Op.iLike]: `%${filter.genre}%`,
        },
      });
    }

    if (filter.director) {
      where[Op.or].push({
        '$director.dir_name$': {
          [Op.iLike]: `%${filter.director}%`,
        },
      });
    }

    return Movie.findAll({
      attributes: [
        'movieId',
        'title',
        'thumbnail',
        'duration',
        'year',
        'trailer',
        'synopsis',
      ],
      include: [
        {
          model: Director,
          as: 'director',
          required: true,
          attributes: ['directorId', 'name'],
        },
        {
          model: Genre,
          as: 'genre',
          required: true,
          attributes: ['genreId', 'name'],
        },
      ],
      where:
        where[Op.or].length > 0
          ? where
          : {
              deletedAt: {
                [Op.eq]: null,
              },
            },
    });
  },

  async findById(movieId) {
    return Movie.findOne({
      where: {
        movieId,
        deletedAt: null,
      },
    });
  },

  async findByAllField(movieId) {
    return Movie.findOne({
      attributes: [
        'movieId',
        'title',
        'thumbnail',
        'duration',
        'year',
        'trailer',
        'synopsis',
      ],
      include: [
        {
          model: Director,
          as: 'director',
          required: true,
          attributes: ['directorId', 'name'],
        },
        {
          model: Genre,
          as: 'genre',
          required: true,
          attributes: ['genreId', 'name'],
        },
      ],
      where: {
        movieId,
        deletedAt: null,
      },
    });
  },
};
