const { Op } = require('sequelize');
const Vote = require('../models/vote');

module.exports = {
  async create(data) {
    return Vote.create(data);
  },

  async findVoteByUserAndMovie(movieId, userId) {
    return Vote.findOne({
      where: {
        [Op.and]: [
          {
            movieId,
          },
          {
            userId,
          },
        ],
      },
    });
  },
  async findVoteById(movieId) {
    return Vote.findAll({
      where: {
        [Op.and]: [
          {
            movieId,
          },
        ],
      },
    });
  },
};
