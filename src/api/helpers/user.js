module.exports = {
  async find(userId, user) {
    const userFind = await user.findByIdWithAllField(userId);

    if (!userFind) {
      return false;
    }

    return true;
  },

  async findAdmin(userId, user) {
    const userFind = await user.findAdmin(userId);

    if (!userFind) {
      return false;
    }

    return true;
  },

  async findByEmail(email, user) {
    const userFind = await user.findByEmail(email);

    if (!userFind) {
      return false;
    }

    return true;
  },
};
