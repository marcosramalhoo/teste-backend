const ValidationError = require('../errors/validationError');

module.exports = {
  validatorClientMovie(movie) {
    if (!movie.title) {
      throw new ValidationError('Nome do filme é obrigatório');
    }
    if (!movie.thumbnail) {
      throw new ValidationError('Thumbnail do filme é obrigatório');
    }
    if (!movie.duration) {
      throw new ValidationError('Duração do filme é obrigatório');
    }
    if (!movie.year) {
      throw new ValidationError('Ano de lançamento do filme é obrigatório');
    }
    if (!movie.synopsis) {
      throw new ValidationError('Sinopse do filme é obrigatório');
    }
    if (!movie.trailer) {
      throw new ValidationError('Trailer do filme é obrigatório');
    }
    if (!movie.directorId) {
      throw new ValidationError('Diretor do filme é obrigatório');
    }
    if (!movie.genreId) {
      throw new ValidationError('Gênero do filme é obrigatório');
    }
  },
};
