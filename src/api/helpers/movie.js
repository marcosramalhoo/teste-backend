module.exports = {
  async find(movieId, movie) {
    const movieFind = await movie.findById(movieId);

    if (!movieFind) {
      return false;
    }

    return true;
  },
};
