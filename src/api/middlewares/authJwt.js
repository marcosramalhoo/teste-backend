module.exports = (app) => {
  const isAdmin = (req, res, next) => {
    app.api.services.user.findByIdWithAllField(req.user.id).then((user) => {
      if (user.isAdmin !== 'S') {
        res.status(403).send({
          message: 'Permission denied',
        });
        return;
      }

      next();
    });
  };

  const isUser = (req, res, next) => {
    app.api.services.user.findByIdWithAllField(req.user.id).then((user) => {
      if (user.isAdmin === 'S') {
        res.status(403).send({
          message: 'O seu nível de acesso não permite realizar votação',
        });
        return;
      }

      next();
    });
  };

  return { isAdmin, isUser };
};
