const user = require('../helpers/user');

const ValidationError = require('../errors/validationError');

module.exports = (app) => {
  const create = async (req, res, next) => {
    try {
      const admin = req.body;
      if (!admin.name) {
        throw new ValidationError('Nome é obrigatório');
      }

      if (!admin.email) {
        throw new ValidationError('Email é obrigatório');
      }

      if (!admin.password) {
        throw new ValidationError('Senha é obrigatório');
      }

      if (user.findByEmail(admin.email, app.api.services.user)) {
        throw new ValidationError('Este email já foi cadastrado');
      }

      admin.password = app.api.services.user.hashPassword(admin.password);
      const response = await app.api.services.user.create({
        ...admin,
        isAdmin: 'S',
      });

      return res.status(201).json({
        userId: response.userId,
        name: response.name,
        email: response.email,
        username: response.username,
      });
    } catch (error) {
      return next(error);
    }
  };

  const update = async (req, res, next) => {
    try {
      if (!(await user.findAdmin(req.params.id, app.api.services.user))) {
        return res.status(404).json({ message: 'Usuário não encontrado' });
      }

      const response = await app.api.services.user.update(
        req.params.id,
        req.body,
      );

      delete req.body.email;
      delete req.body.password;

      return res.status(200).json({
        userId: response[1].userId,
        name: response[1].name,
        email: response[1].email,
        username: response[1].username,
      });
    } catch (error) {
      return next(error);
    }
  };

  const remove = async (req, res, next) => {
    try {
      if (!(await user.findAdmin(req.params.id, app.api.services.user))) {
        return res.status(404).json({ message: 'Usuário não encontrado' });
      }

      await app.api.services.user.delete(req.params.id);

      return res.status(204).json();
    } catch (error) {
      return next(error);
    }
  };

  return { create, update, remove };
};
