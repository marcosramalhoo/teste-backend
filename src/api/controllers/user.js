const user = require('../helpers/user');

module.exports = (app) => {
  const update = async (req, res, next) => {
    try {
      if (!(await user.find(req.params.id, app.api.services.user))) {
        return res.status(404).json({ message: 'Usuário não encontrado' });
      }

      delete req.body.password;
      delete req.body.email;

      const response = await app.api.services.user.update(
        req.params.id,
        req.body,
      );

      return res.status(200).json({
        userId: response[1].userId,
        name: response[1].name,
        email: response[1].email,
        username: response[1].username,
        createdAt: response[1].createdAt,
        updatedAt: response[1].updatedAt,
      });
    } catch (error) {
      return next(error);
    }
  };

  const remove = async (req, res, next) => {
    try {
      if (!(await user.find(req.params.id, app.api.services.user))) {
        return res.status(404).json({ message: 'Usuário não encontrado' });
      }

      await app.api.services.user.delete(req.params.id);

      return res.status(204).json();
    } catch (error) {
      return next(error);
    }
  };

  return { update, remove };
};
