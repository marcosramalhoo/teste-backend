const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../../config/env');
const ValidationError = require('../errors/validationError');

const userHelper = require('../helpers/user');

module.exports = (app) => {
  const signup = async (req, res, next) => {
    try {
      const signUp = req.body;

      if (!signUp.name) {
        throw new ValidationError('Nome é obrigatório');
      }

      if (!signUp.email) {
        throw new ValidationError('Email é obrigatório');
      }

      if (!signUp.password) {
        throw new ValidationError('Senha é obrigatório');
      }

      if (await userHelper.findByEmail(signUp.email, app.api.services.user)) {
        throw new ValidationError('Usuário com este email já existe');
      }

      signUp.password = app.api.services.user.hashPassword(signUp.password);
      const response = await app.api.services.user.create(signUp);

      return res.status(201).json({
        userId: response.userId,
        name: response.name,
        email: response.email,
        username: response.username,
        createdAt: response.createdAt,
      });
    } catch (error) {
      return next(error);
    }
  };

  const signin = async (req, res, next) => {
    try {
      const user = await app.api.services.user.findByEmail(req.body.email);

      if (!user) {
        return res.status(401).json({ message: 'Credenciais inválidas!' });
      }

      if (!bcrypt.compareSync(req.body.password, user.password)) {
        return res.status(401).json({ message: 'Credenciais inválidas!' });
      }

      const payload = {
        id: user.userId,
        email: user.email,
        role: user.isAdmin === 'S' ? 'ADMIN' : 'USER',
      };

      const token = jwt.sign(payload, config.secret_token, {
        expiresIn: 86400,
      });

      return res.status(200).json({
        message: 'Autenticado com sucesso',
        id: user.userId,
        name: user.name,
        email: user.email,
        token,
      });
    } catch (error) {
      return next(error);
    }
  };

  return { signup, signin };
};
