const movie = require('../helpers/movie');
const validators = require('../helpers/validators');
const ValidationError = require('../errors/validationError');

module.exports = (app) => {
  const create = async (req, res, next) => {
    try {
      validators.validatorClientMovie(req.body);
      const response = await app.api.services.movie.create(req.body);

      return res.status(201).json(response);
    } catch (error) {
      return next(error);
    }
  };

  const remove = async (req, res, next) => {
    try {
      if (!(await movie.find(req.params.id, app.api.services.movie))) {
        return res.status(404).json({ message: 'Filme não encontrado!' });
      }

      await app.api.services.movie.delete(req.params.id);

      return res.status(204).json();
    } catch (error) {
      return next(error);
    }
  };

  const update = async (req, res, next) => {
    try {
      if (!(await movie.find(req.params.id, app.api.services.movie))) {
        return res.status(404).json({ message: 'Filme não encontrado!' });
      }

      const response = await app.api.services.movie.update(
        req.params.id,
        req.body,
      );

      return res.status(200).json(response[1]);
    } catch (error) {
      return next(error);
    }
  };

  const index = async (req, res, next) => {
    try {
      const { title, director, genre } = req.query;

      const response = await app.api.services.movie.findAll({
        title,
        director,
        genre,
      });

      return res.status(response.length <= 0 ? 404 : 200).json(response);
    } catch (error) {
      return next(error);
    }
  };

  const show = async (req, res, next) => {
    try {
      if (!(await movie.find(req.params.id, app.api.services.movie))) {
        return res.status(404).json({ message: 'Filme não encontrado!' });
      }

      const response = await app.api.services.movie.findByAllField(
        parseInt(req.params.id),
      );

      const votesComputed = await app.api.services.vote.findVoteById(
        parseInt(req.params.id),
      );

      let rating = 0;

      if (votesComputed.length > 0) {
        votesComputed.map((v) => {
          rating += v.rating;

          return v;
        });
      }

      return res.status(200).json({
        ...response.dataValues,
        rating: rating === 0 ? 'Sem avaliação' : rating / votesComputed.length,
      });
    } catch (error) {
      return next(error);
    }
  };

  const votes = async (req, res, next) => {
    try {
      const { id } = req.user;
      const { movieId, rating } = req.body;

      if (!movieId || !rating) {
        throw new ValidationError('Informe a avaliação e o filme');
      }

      if (rating > 4 || rating < 0) {
        throw new ValidationError('Nota da avaliação permitida: 0 a 4');
      }

      const votesRealized = await app.api.services.vote.findVoteByUserAndMovie(
        movieId,
        id,
      );

      if (votesRealized) {
        throw new ValidationError(
          'Você ja avaliou esse filme. Ressaltamos que é permitido um voto por filme',
        );
      }

      await app.api.services.vote.create({ movieId, rating, userId: id });

      return res
        .status(201)
        .json({ message: 'Agradecemos sua avalição :)', votesRealized });
    } catch (error) {
      return next(error);
    }
  };

  return {
    create,
    remove,
    update,
    index,
    votes,
    show,
  };
};
