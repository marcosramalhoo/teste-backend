const { Model, DataTypes } = require('sequelize');

class Genre extends Model {
  static init(sequelize) {
    super.init(
      {
        genreId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'gen_id',
        },
        name: {
          type: DataTypes.STRING,
          field: 'gen_name',
        },
        createdAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        updatedAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deletedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
      },
      {
        sequelize,
        tableName: 'genres',
      },
    );
  }

  static associate(models) {
    this.hasMany(models.Movie, { foreignKey: 'gen_id', as: 'genres' });
  }
}

module.exports = Genre;
