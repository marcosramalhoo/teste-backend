const { Model, DataTypes } = require('sequelize');

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        userId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'usu_id',
        },
        name: {
          type: DataTypes.STRING,
          field: 'usu_name',
        },
        email: {
          type: DataTypes.STRING,
          field: 'usu_email',
        },
        password: {
          type: DataTypes.STRING,
          field: 'usu_password',
        },
        username: {
          type: DataTypes.STRING,
          field: 'usu_username',
          defaultValue: '',
        },
        isAdmin: {
          type: DataTypes.STRING,
          defaultValue: 'N',
          field: 'usu_isAdmin',
        },
        createdAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deletedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
        updatedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
      },
      {
        sequelize,
        tableName: 'users',
      },
    );
  }

  static associate(models) {
    this.belongsToMany(models.Movie, {
      foreignKey: 'usu_id',
      through: 'votes',
      as: 'movies',
    });
  }
}

module.exports = User;
