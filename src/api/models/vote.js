const { Model, DataTypes } = require('sequelize');

class Vote extends Model {
  static init(sequelize) {
    super.init(
      {
        voteId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'id',
        },
        userId: {
          type: DataTypes.INTEGER,
          foreignKey: true,
          field: 'usu_id',
        },
        movieId: {
          type: DataTypes.INTEGER,
          foreignKey: true,
          field: 'mov_id',
        },
        rating: {
          type: DataTypes.INTEGER,
          field: 'rating',
        },
        createdAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deletedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
        updatedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
      },
      {
        sequelize,
        tableName: 'votes',
      },
    );
  }

  static associate(models) {
    this.belongsTo(models.Movie, { foreignKey: 'mov_id', as: 'movie' });
    this.belongsTo(models.User, { foreignKey: 'usu_id', as: 'user' });
  }
}

module.exports = Vote;
