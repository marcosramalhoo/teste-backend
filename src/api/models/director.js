const { Model, DataTypes } = require('sequelize');

class Director extends Model {
  static init(sequelize) {
    super.init(
      {
        directorId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'dir_id',
        },
        name: {
          type: DataTypes.STRING,
          field: 'dir_name',
        },
        createdAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        updatedAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deletedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
      },
      {
        sequelize,
        tableName: 'directors',
      },
    );
  }

  static associate(models) {
    this.hasMany(models.Movie, { foreignKey: 'dir_id', as: 'directors' });
  }
}

module.exports = Director;
