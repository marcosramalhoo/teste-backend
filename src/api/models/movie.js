const { Model, DataTypes } = require('sequelize');

class Movie extends Model {
  static init(sequelize) {
    super.init(
      {
        movieId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          field: 'mov_id',
        },
        title: {
          type: DataTypes.STRING,
          field: 'mov_title',
        },
        thumbnail: {
          type: DataTypes.STRING,
          field: 'mov_url_thumbnail',
        },
        duration: {
          type: DataTypes.INTEGER,
          field: 'mov_duration',
        },
        year: {
          type: DataTypes.INTEGER,
          field: 'mov_year',
        },
        synopsis: {
          type: DataTypes.TEXT,
          field: 'mov_synopsis',
        },
        trailer: {
          type: DataTypes.STRING,
          field: 'mov_url_trailer',
        },
        createdAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        genreId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          field: 'gen_id',
        },
        directorId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          field: 'dir_id',
        },
        updatedAt: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
        },
        deletedAt: {
          type: DataTypes.DATE,
          defaultValue: null,
        },
      },
      {
        sequelize,
        tableName: 'movies',
      },
    );
  }

  static associate(models) {
    this.belongsTo(models.Genre, { foreignKey: 'gen_id', as: 'genre' });
    this.belongsTo(models.Director, { foreignKey: 'dir_id', as: 'director' });
    this.belongsToMany(models.User, {
      foreignKey: 'mov_id',
      through: 'votes',
      as: 'users',
    });
  }
}

module.exports = Movie;
