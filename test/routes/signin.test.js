const request = require('supertest');
const app = require('../../src/app');

describe('Signin Controller', () => {
  test('Should return 200 if authenticated', () => {
    const email = `${Date.now()}@email.com`;
    const username = `${Date.now()}user`;

    return app.api.services.user
      .create({
        name: 'Marcos',
        password: app.api.services.user.hashPassword('123456'),
        username,
        email,
      })
      .then(() => {
        return request(app)
          .post('/auth/signin')
          .send({
            password: '123456',
            email,
          })
          .then((res) => {
            expect(res.status).toBe(200);
            expect(res.body.message).toBe('Autenticado com sucesso');
            expect(res.body.email).toBe(email);
            expect(res.body).toHaveProperty('token');
          });
      });
  });

  test('Should return 401 if wrong password', () => {
    return request(app)
      .post('/auth/signin')
      .send({
        password: '1234567',
        email: `${Date.now()}@email.com`,
      })
      .then((res) => {
        expect(res.status).toBe(401);
        expect(res.body.message).toBe('Credenciais inválidas!');
      });
  });
});
