const request = require('supertest');
const jwt = require('jsonwebtoken');
const app = require('../../src/app');
const config = require('../../src/config/env');

let user;

beforeAll(async () => {
  user = await app.api.services.user.create({
    name: 'User Add',
    password: '123456',
    username: `${Date.now()}user`,
    email: `${Date.now()}@mail.com`,
    isAdmin: 'S',
  });

  user.token = jwt.sign(
    {
      id: user.userId,
      email: user.email,
      role: user.isAdmin === 'S' ? 'ADMIN' : 'USER',
    },
    config.secret_token,
    {
      expiresIn: 86400,
    },
  );
});

describe('User Controller', () => {
  test('Should return 200 if updated with success', () => {
    return request(app)
      .put(`/api/v1/users/${user.userId}`)
      .auth(user.token, { type: 'bearer' })
      .send({
        name: 'Name updated',
      })
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.name).toBe('Name updated');
      });
  });

  test('Should return 204 if deleted with success', () => {
    return request(app)
      .del(`/api/v1/users/${user.userId}`)
      .auth(user.token, { type: 'bearer' })
      .then((res) => {
        expect(res.status).toBe(204);
      });
  });
});
