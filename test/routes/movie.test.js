const request = require('supertest');
const jwt = require('jsonwebtoken');

const app = require('../../src/app');
const config = require('../../src/config/env');

let director;
let genre;
let movie;
let user;
beforeAll(async () => {
  user = await app.api.services.user.create({
    name: 'User Add',
    password: '123456',
    username: `${Date.now()}user`,
    email: `${Date.now()}@mail.com`,
    isAdmin: 'S',
  });

  user.token = jwt.sign(
    {
      id: user.userId,
      email: user.email,
      role: user.isAdmin === 'S' ? 'ADMIN' : 'USER',
    },
    config.secret_token,
    {
      expiresIn: 86400,
    },
  );

  director = await app.api.services.director.create({
    name: 'Diretor 1',
  });

  genre = await app.api.services.genre.create({
    name: 'Genero 1',
  });

  movie = await app.api.services.movie.create({
    title: 'Filme 1 Amazonia',
    thumbnail:
      'https://viajarpraonde.files.wordpress.com/2015/03/filme-eurotrip.jpg',
    duration: 90,
    year: 2020,
    synopsis:
      'Eurotrip é uma comédia que conta a história de cinco amigos que viajam pela Europa em busca da garota perfeita que um deles conheceu pela internet. A história se passa em Paris, Roma, Berlim, Amsterdam, Bratislava e Londres, mas foi principalmente gravada em Praga. Eu particularmente, adorei esse filme! Achei muito engraçado amazonia',
    trailer: 'https://www.youtube.com/watch?v=HqnwVAaEu40',
    directorId: director.directorId,
    genreId: genre.genreId,
  });
});

describe('Movie Controller', () => {
  test('Should return 201 if all data has been provided', () => {
    return request(app)
      .post('/api/v1/movies')
      .auth(user.token, { type: 'bearer' })
      .send({
        title: 'Filme 1',
        thumbnail:
          'https://viajarpraonde.files.wordpress.com/2015/03/filme-eurotrip.jpg',
        duration: 90,
        year: 2020,
        synopsis:
          'Eurotrip é uma comédia que conta a história de cinco amigos que viajam pela Europa em busca da garota perfeita que um deles conheceu pela internet. A história se passa em Paris, Roma, Berlim, Amsterdam, Bratislava e Londres, mas foi principalmente gravada em Praga. Eu particularmente, adorei esse filme! Achei muito engraçado!',
        trailer: 'https://www.youtube.com/watch?v=HqnwVAaEu40',
        directorId: director.directorId,
        genreId: genre.genreId,
      })
      .then((res) => {
        expect(res.status).toBe(201);
        expect(res.body.title).toBe('Filme 1');
        expect(res.body.duration).toBe(90);
        expect(res.body.year).toBe(2020);
        expect(res.body.genreId).toBe(genre.genreId);
        expect(res.body.directorId).toBe(director.directorId);
      });
  });

  test('Should return 204 if deleted with success', () => {
    return request(app)
      .del(`/api/v1/movies/${movie.movieId}`)
      .auth(user.token, { type: 'bearer' })
      .then((res) => {
        expect(res.status).toBe(204);
      });
  });

  test('Should return 200 if updated with success', () => {
    return app.api.services.movie
      .create({
        title: 'Filme 1 Amazonia',
        thumbnail:
          'https://viajarpraonde.files.wordpress.com/2015/03/filme-eurotrip.jpg',
        duration: 90,
        year: 2020,
        synopsis:
          'Eurotrip é uma comédia que conta a história de cinco amigos que viajam pela Europa em busca da garota perfeita que um deles conheceu pela internet. A história se passa em Paris, Roma, Berlim, Amsterdam, Bratislava e Londres, mas foi principalmente gravada em Praga. Eu particularmente, adorei esse filme! Achei muito engraçado amazonia',
        trailer: 'https://www.youtube.com/watch?v=HqnwVAaEu40',
        directorId: director.directorId,
        genreId: genre.genreId,
      })
      .then((result) => {
        return request(app)
          .put(`/api/v1/movies/${result.movieId}`)
          .auth(user.token, { type: 'bearer' })
          .send({
            title: 'Title updated',
            duration: 120,
            year: 2018,
          })
          .then((res) => {
            expect(res.status).toBe(200);
            expect(res.body.title).toBe('Title updated');
            expect(res.body.duration).toBe(120);
            expect(res.body.year).toBe(2018);
          });
      });
  });

  test('Should list all movies', () => {
    return request(app)
      .get('/api/v1/movies')
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.length).toBeGreaterThanOrEqual(0);
      });
  });

  test('Should list all movies by genre', () => {
    return app.api.services.movie
      .create({
        title: 'Amazonia',
        thumbnail:
          'https://viajarpraonde.files.wordpress.com/2015/03/filme-eurotrip.jpg',
        duration: 90,
        year: 2020,
        synopsis: 'Um filme muito bom!',
        trailer: 'https://www.youtube.com/watch?v=HqnwVAaEu40',
        directorId: director.directorId,
        genreId: genre.genreId,
      })
      .then(() => {
        return request(app)
          .get('/api/v1/movies?genre=genero')
          .then((res) => {
            expect(res.status).toBe(200);
            expect(res.body[0].genre.name).toBe('Genero 1');
          });
      });
  });

  test('Should list all movies by director', () => {
    return app.api.services.movie
      .create({
        title: 'Amazonia',
        thumbnail:
          'https://viajarpraonde.files.wordpress.com/2015/03/filme-eurotrip.jpg',
        duration: 90,
        year: 2020,
        synopsis: 'Um filme muito bom!',
        trailer: 'https://www.youtube.com/watch?v=HqnwVAaEu40',
        directorId: director.directorId,
        genreId: genre.genreId,
      })
      .then(() => {
        return request(app)
          .get('/api/v1/movies?director=diretor')
          .then((res) => {
            expect(res.status).toBe(200);
            expect(res.body[0].director.name).toBe('Diretor 1');
          });
      });
  });

  test('Should return 401 if no provided token valid', () => {
    return request(app)
      .post('/api/v1/movies')
      .send({
        title: 'Filme 2',
        thumbnail:
          'https://viajarpraonde.files.wordpress.com/2015/03/filme-eurotrip.jpg',
        duration: 90,
        year: 2020,
        synopsis:
          'Eurotrip é uma comédia que conta a história de cinco amigos que viajam pela Europa em busca da garota perfeita que um deles conheceu pela internet. A história se passa em Paris, Roma, Berlim, Amsterdam, Bratislava e Londres, mas foi principalmente gravada em Praga. Eu particularmente, adorei esse filme! Achei muito engraçado!',
        trailer: 'https://www.youtube.com/watch?v=HqnwVAaEu40',
        directorId: director.directorId,
        genreId: genre.genreId,
      })
      .then((res) => {
        expect(res.status).toBe(401);
      });
  });
});
