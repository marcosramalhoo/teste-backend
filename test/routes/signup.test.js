const request = require('supertest');
const app = require('../../src/app');

describe('Signup Controller', () => {
  test('Should return 201 if all data has been provided', () => {
    const email = `${Date.now()}@mail.com`;
    const username = `${Date.now()}user`;
    return request(app)
      .post('/auth/signup')
      .send({
        name: 'Marcos',
        password: '123456',
        username,
        email,
      })
      .then((res) => {
        expect(res.status).toBe(201);
        expect(res.body.name).toBe('Marcos');
        expect(res.body.username).toBe(username);
        expect(res.body.email).toBe(email);
        expect(res.body).not.toHaveProperty('password');
      });
  });

  test('Should encrypt password', async () => {
    const res = await request(app)
      .post('/auth/signup')
      .send({
        name: 'Marcos',
        password: '123456',
        username: `${Date.now()}user`,
        email: `${Date.now()}@mail.com`,
      });
    expect(res.status).toBe(201);

    const { userId } = res.body;

    const user = await app.api.services.user.findByIdWithAllField(userId);
    expect(user.password).not.toBeUndefined();
    expect(user.password).not.toBe('123456');
  });
});
