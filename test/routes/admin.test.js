const request = require('supertest');
const jwt = require('jsonwebtoken');
const app = require('../../src/app');
const config = require('../../src/config/env');

let admin;

beforeAll(async () => {
  admin = await app.api.services.user.create({
    name: 'Admin',
    password: '123456',
    username: `${Date.now()}admin`,
    email: `${Date.now()}@mail.com`,
    isAdmin: 'S',
  });

  admin.token = jwt.sign(
    {
      id: admin.userId,
      email: admin.email,
      role: admin.isAdmin === 'S' ? 'ADMIN' : 'USER',
    },
    config.secret_token,
    {
      expiresIn: 86400,
    },
  );
});

describe('Admin Controller', () => {
  test('Should return 200 if updated with success', () => {
    return app.api.services.user
      .create({
        name: 'Admin',
        password: '123456',
        username: `${Date.now()}admin`,
        email: `${Date.now()}@mail.com`,
        isAdmin: 'S',
      })
      .then((result) => {
        return request(app)
          .put(`/api/v1/admins/${result.userId}`)
          .auth(admin.token, { type: 'bearer' })
          .send({
            name: 'Name updated',
          })
          .then((res) => {
            expect(res.status).toBe(200);
            expect(res.body.name).toBe('Name updated');
          });
      });
  });

  test('Should return 204 if deleted with success', () => {
    return request(app)
      .del(`/api/v1/admins/${admin.userId}`)
      .auth(admin.token, { type: 'bearer' })
      .then((res) => {
        expect(res.status).toBe(204);
      });
  });
});
